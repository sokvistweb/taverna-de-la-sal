<!DOCTYPE html>
<!--[if lt IE 7 ]> <html class="ie ie6 ie-lt10 ie-lt9 ie-lt8 ie-lt7 no-js" lang="es"> <![endif]-->
<!--[if IE 7 ]>    <html class="ie ie7 ie-lt10 ie-lt9 ie-lt8 no-js" lang="es"> <![endif]-->
<!--[if IE 8 ]>    <html class="ie ie8 ie-lt10 ie-lt9 no-js" lang="es"> <![endif]-->
<!--[if IE 9 ]>    <html class="ie ie9 ie-lt10 no-js" lang="es"> <![endif]-->
<!--[if gt IE 9]><!--><html class="no-js" lang="es"><!--<![endif]-->
<head>
    <!-- Global site tag (gtag.js) - Google Analytics -->
    <script async src="https://www.googletagmanager.com/gtag/js?id=UA-47544981-19"></script>
    <script>
      window.dataLayer = window.dataLayer || [];
      function gtag(){dataLayer.push(arguments);}
      gtag('js', new Date());

      gtag('config', 'UA-47544981-19');
    </script>
    
    <meta charset="utf-8">
	<title>Taverna de la Sal · L'Escala</title>
    <meta http-equiv="X-UA-Compatible" content="IE=edge" />
	<meta name="description" content="Hotel con encanto en la Costa Brava, situado en el casco antiguo del municipio de la Escala, Alt Empordà, a pocos metros de la Playa de las Barcas, antiguo puerto de pescadores." />
    <meta name="keywords" content="hotel, Costa Brava, Empordà, playa" />
    <meta name="author" content="sokvist.com">

    <meta name="viewport" content="width=device-width, initial-scale=1">

    <link rel="shortcut icon" href="favicon.ico" />
    <link rel="apple-touch-icon" href="apple-touch-icon.png" />
    <link rel="apple-touch-icon-precomposed" href="apple-touch-icon-precomposed.png">
    <link href="https://fonts.googleapis.com/css?family=Roboto:300,700" rel="stylesheet">
	<link rel="stylesheet" type="text/css" href="assets/css/style.css">
    
    <link rel="canonical" href="http://tavernadelasal.com/es">
    <link hreflang="ca" href="http://tavernadelasal.com/" rel="alternate">
    <link hreflang="en" href="http://tavernadelasal.com/en/" rel="alternate">
    <link hreflang="fr" href="http://tavernadelasal.com/fr/" rel="alternate">
    
    <!-- Facebook Open Graph -->
    <meta property="og:type" content="website">
    <meta property="og:url" content="http://tavernadelasal.com/">
    <meta property="og:title" content="Taverna de la Sal · L'Escala">
    <meta property="og:image" content="assets/images/og-image.jpg">
    <meta property="og:description" content="Hotel con encanto en la Costa Brava, situado en el casco antiguo del municipio de la Escala, Alt Empordà, a pocos metros de la Playa de las Barcas, antiguo puerto de pescadores.">
    <meta property="og:site_name" content="Taverna de la Sal · L'Escala">
    <meta property="og:locale" content="es">
    <meta property="og:image:width" content="1200">
    <meta property="og:image:height" content="630">
    
    <!-- Twitter Card 
    <meta name="twitter:card" content="summary">
    <meta name="twitter:site" content="@expoprimats">
    <meta name="twitter:creator" content="@SokvistWeb">
    <meta name="twitter:url" content="http://tavernadelasal.com/">
    <meta name="twitter:title" content="Taverna de la Sal · L'Escala">
    <meta name="twitter:description" content="Hotel con encanto en la Costa Brava, situado en el casco antiguo del municipio de la Escala, Alt Empordà, a pocos metros de la Playa de las Barcas, antiguo puerto de pescadores.">
    <meta name="twitter:image" content="assets/images/og-image.jpg">-->
    
    <script async src="assets/js/vendor/modernizr.js"></script>
</head>
    
    
<body class="home" data-region="emea" data-channelcode="tavernadelasaldirect">

    <!-- Header -->
    <header id="header">

        <!-- Logo -->
        <?php include 'includes/logo.php';?>
        
        <!-- Nav -->
        <nav class="main-nav smart-nav icon ion-ios-more" role="navigation">
            <div>
            <ul id="nav">
                <li><a href="#hotel">Hotel</a></li>
                <li><a href="#habitacions">Habitaciones</a></li>
                <li><a href="#serveis">Servicios</a></li>
                <li><a href="#galeria">Galería</a></li>
                <li><a href="#contacte">Contacto</a></li>
            </ul>
            </div>
        </nav>
        
        <div class="contact">
            <a href="tel:0034972776278" class="icon ion-iphone"><span> +34 972 776 278</span></a>
            <a href="mailto:info@tavernadelasal.com" class="icon ion-ios-email"><span class="hidden">info@tavernadelasal.com</span></a>
            <a href="https://www.google.es/maps/place/Carrer+de+Santa+M%C3%A0xima,+7,+17130+L'Escala,+Girona/@42.1259022,3.1312329,17z/data=!3m1!4b1!4m5!3m4!1s0x12ba5f26727f0895:0x3f6c33fc81532504!8m2!3d42.1258982!4d3.1334216" class="icon ion-ios-location" title="Veure a Google maps" target="_blank"><span class="hidden">On som</span></a>
            <a href="#" class="lang-switcher icon ion-earth" title="Idioma"><span class="hidden">Idioma</span></a>
        </div>
        
        <div class="language">
            <ul class="qtranxs_language_chooser" id="qtranslate-chooser">
                <li class="lang-ca"><a href="ca" hreflang="ca" title="Català"><span>CA</span></a></li>
                <li class="lang-es active-lang"><a href="es" hreflang="es" title="Español"><span>ES</span></a></li>
                <li class="lang-en"><a href="en" hreflang="en" title="English"><span>EN</span></a></li>
                <li class="lang-fr"><a href="fr" hreflang="fr" title="Français"><span>FR</span></a></li>
            </ul>
            <div class="qtranxs_widget_end"></div>
        </div>
        
    </header>
    
    
    <div class="book-online">
        <h2>Reserva Online</h2>
        
        <!--<div class="getmore modal__trigger"><i class="icon ion-android-done"></i><span>Avantatges</span></div>-->
        
        <form action="#" class="ibe" method="get" data-region="emea" data-channelcode="tavernadelasaldirect" data-widget="property_check_availability" target="_blank">
            <fieldset>
                <div class="field">
                    Entrada:
                    <input name="check_in_date" class="datepicker-input" data-language="es" data-position="top right" data-date-format="dd/mm/yyyy" id="check_in_date" value="" type="text" data-init-val="null">
                    Salida:
                    <input name="check_out_date" class="datepicker-input" data-language="es" data-position="top right" data-date-format="dd/mm/yyyy" id="check_out_date" value="" type="text" data-init-val="null">
                    <input name="commit" value="Reservar" type="submit">
                    
                    <a class="offers" href="popups/offer-es.html" title="Avantatges" rel="modal:open">Ventajas</a>
                </div>
            </fieldset>
        </form>

    </div><!-- /.book-online -->
    
    <!-- Mobile booking button -->
    <a href="#" class="booking-btn ibe" data-region="emea" data-channelcode="tavernadelasaldirect" target="_blank">Reserva Online</a>
    

    <div id="btm-frame">
        <h2>Boutique Hotel Only Adults</h2>
        <nav>
            <ul id="arrow-nav" class="arrow-nav">
                <li><a href="#one" class="icon ion-android-arrow-down down bounce" id="one-btn">More</a></li>
                <li><a href="#two" class="icon ion-android-arrow-down down" id="two-btn">Next</a></li>
            </ul>
        </nav>
    </div>
    

    <div id="lft-frame"></div>
    <div id="rgt-frame"></div>
        

    <!-- Intro -->
    <section id="intro" class="fixed">
        <div class="bg-image-bn"></div>
        <div class="bg-image-color"></div>
    </section>

    <!-- One -->
    <section id="one">
    </section>
        
        
    <div class="modernist"></div>


    <!-- two -->
    <section id="hotel" class="two main style3" data-animate-down="arrow-hide" data-animate-up="arrow-show">
        <div class="container container-narrow">
            <div class="row">
                <div class="column fullwidth">
                    <header>
                        <h1>Taverna de la Sal</h1>
                    </header>
                    <div class="copy">
                        <h2>La Taverna de la Sal es el hotel con encanto en la Costa Brava que estáis buscando</h2>
                        
                        <p>Hotel con encanto en el Ampurdán, situado en el casco antiguo del municipio de la Escala, a 20 metros de la Playa de las Barcas, antiguo puerto de pescadores.</p>
                        
                    </div>
                </div>
            </div> <!-- /.row -->
            
            <div class="row">
                <div class="column">
                    
                    <?php include 'includes/home-slider.php';?>
                    
                </div>
            </div> <!-- /.row -->
            
            <div class="row">
                <div class="column fullwidth">
                    <div class="copy copy-btm">
                        
                        <p>En medio de magníficas playas y de parajes naturales únicos, podréis gozar de una estancia en este acogedor hotel de uso exclusivo para público adulto donde nuestro mayor compromiso es vuestro bienestar.</p>
                        
                        <p>Hotel con encanto en la Costa Brava- Ampurdán- La Escala que consta de 6 preciosas habitaciones con un estilo mediterráneo y que ofrece una cuidada decoración en todos sus detalles. Un rincón de la Costa Brava donde escaparse...</p>
                        
                    </div>
                </div>
            </div> <!-- /.row -->
        </div> <!-- /.container -->
    </section>


    <!-- three -->
    <section id="habitacions" class="three main style3">
        <div class="modernist"></div>
        <div class="container">
            
            <h2>Habitaciones</h2>
            
            <div class="slider" id="basica">
                
                <?php include 'includes/rooms-slider.php';?>
                
                <div class="copy">
                    <h3>Espaciosas y acogedoras habitaciones. Descanso, confort y buenas sensaciones</h3>
                    
                    <p>La Taverna de la Sal es un hotel a pie de playa, romántico y con encanto de 6 habitaciones dobles, cada una de ellas con su personalidad y cuidadas con todos los detalles. Hay habitaciones que son convertibles en triples. Todas las habitaciones son para público adulto a partir de 14 años que desee una estancia tranquila a pocos metros del mar, en pleno casco antiguo de la Escala.</p>
                    
                    <p>Amplias habitaciones con cama de matrimonio. Todas disponen de balcón con vistas laterales al mar y estan equipadas con:</p>
                    
                    <ul class="custom-li">
                    <li>Cama doble 2,00 x 1,80</li>
                    <li>Aire acondicionado y calefacción</li>
                    <li>Baño completo con ducha</li>
                    <li>TV pantalla plana (canales internacionales)</li>
                    <li>Caja fuerte</li>
                    <li>Mini nevera</li>
                    <li>Secador de pelo</li>
                    <li>Kettle para café y té</li>
                    <li>Internet Wi-Fi gratis</li>
                    <li>Amenities de baño</li>
                    </ul>
                    
                    <p>Habitaciones 100% libres de humos.</p>
                    
                    <p>No están permitidas las mascotas.</p>

                    <div class="buttons">
                        <a class="booknow ibe" href="#" data-region="emea" data-channelcode="tavernadelasaldirect" target="_blank" title="Página de reservas">Reserva ahora</a>
                        <a class="offers offers-rooms" href="popups/offer-es.html" title="Avantatges" rel="modal:open">Ventajas</a>
                    </div>

                </div>
            </div> <!-- .slider -->
            
            
        </div> <!-- /.container -->
    </section>


    <!-- four -->
    <section id="serveis" class="four main style3">
        <div class="modernist"></div>
        <div class="container">
            <div class="row">
                <div class="column">
                    <header>
                        <h2>Servicios</h2>
                    </header>

                    <div class="copy">
                        
                        <h3>Os ofrecemos todo lo necesario porqué queremos que vuestra estada en la Escala sea única</h3>
                        
                        <h4>Restaurante Taverna de la Sal</h4>
                        <p>El Restaurante está situado a la planta baja y a la primera planta del edificio. Si bién nuestra especialidad es la carne a la brasa, también ofrecemos una amplia carta de platos basados en la cocina tradicional catalana, trabajando siempre con productos frescos y de proximidad.</p>
                        
                        <h4>La Terraza</h4>
                        <p>En la cubierta de la Taverna de la Sal se ubica la Terraza, un solarium por los que prefieren tomar el sol en el hotel o simplemente gozar de fantásticas puestas de sol que os dejarán enamorados.</p>
                        
                        <h4>Desayuno</h4>
                        <p>El desayuno es tipo buffet y se sirve en horario de 8h a 10,30h.</p>
                        
                        <h4>Atención al cliente</h4>
                        <p>Hablamos diferentes idiomas y con dedicación y siempre con una sonrisa, ofrecemos una atención muy personalizada adaptada a las necesidades de cada cliente.</p>
                        
                        <p>Es siempre un placer poder recomendar a nuestros clientes los mejores itinerarios locales y de la zona, los puntos de interés turístico y los eventos culturales que tienen lugar durante toda la estancia.</p>
                        
                        <h4>Bar-Cafetería</h4>
                        <p>A lo largo del día, por los clientes que lo deseen, se mantendrá abierto el servicio de bar-cafetería situado al mismo restaurante. Disponemos de una carta de bebidas y cocktails que podréis disfrutar cuando más os venga de gusto.</p>
                        
                        <h4>Parking</h4>
                        <p>A unos 250 m del hotel disponemos de un servicio de parking subterráneo. Es necesaria la reserva previa y el servicio está sujeto a disponiblidad.</p>

                        <p>El coste es de 15,00€/noche (IVA incluido).</p>

                        <p>Hay que solicitarlo en el momento de hacer la reserva en el apartado COMENTARIOS. De esta forma les daremos prioridad en función de las solicitudes y disponibilidad del día de llegada.</p>
                        
                    </div>
                    
                </div>
            </div> <!-- /.row -->
        </div> <!-- /.container -->
    </section>
    
    
    <!-- five -->
    <section id="galeria" class="three main style3">
        <div class="modernist"></div>
        <div class="container">
                    
            <h2>Galería</h2>

            <div class="copy">
                <p>El hotel con encanto en la Costa Brava Taverna de la Sal se situa en un entorno privilegiado por los amantes del mar y la naturaleza. El Ampurdán en general i la Escala en particular combinan a la perfeccción preciosas playas, turismo activo, diversidad de deportes náuticos, gastronomía de calidad, cultura y tradición.</p>
            </div>
            
            <?php include 'includes/gallery.php';?>
            
        </div>
    </section>


    <!-- Contact -->
    <aside id="contacte" class="contacte main style3">
        <div class="modernist modernist-light"></div>
        <div class="container">
            
            <h2>Contacto</h2>
            
            <div class="contact-footer">

                <div class="row">
                    <div class="column lft-column">
                        <h3>Taverna de la Sal</h3>
                        <p>Santa Màxima, 7<br>
                        L'Escala,<br>
                        17130 Girona<br>
                        Tel:<a href="tel:0034972776278"> 0034 972 776 278</a><br>
                        <a href="mailto:info@tavernadelasal.com"><span>info@tavernadelasal.com</span></a>
                        </p>
                    </div>
                    <div class="column ctr-column">
                        <!-- Nav Footer -->
                        <nav class="footer-nav" role="navigation">
                            <ul>
                                <li><a href="#hotel">Hotel</a></li>
                                <li><a href="#habitacions">Habitaciones</a></li>
                                <li><a href="#serveis">Servicios</a></li>
                                <li><a href="#galeria">Galería</a></li>
                                <li><a href="#contacte">Contacto</a></li>
                            </ul>
                        </nav>
                    </div>
                    <div class="column rgt-column">
                        <!-- Mailchimp Subscribe Form -->
                        <div id="mc_embed_signup" class="subscribe-form">
                            <h3>Suscríbete y te informaremos de nuestras ofertas y novedades</h3>
                            <form action="" method="post" id="mc-embedded-subscribe-form" name="mc-embedded-subscribe-form" class="validate" target="_blank" novalidate="">

                                <div class="mc-field-group">
                                    <input type="email" value="" name="EMAIL" class="required email" id="mce-EMAIL" placeholder="Email">
                                </div>

                                <input type="text" value="" name="FNAME" class="" id="mce-FNAME" placeholder="Nombre">
                                <div id="mce-responses" class="clear">
                                    <div class="response" id="mce-error-response"></div>
                                    <div class="response" id="mce-success-response"></div>
                                </div>    <!-- real people should not fill this in and expect good things - do not remove this or risk form bot signups-->
                                <div id="real-people" aria-hidden="true"><input type="text" name="b_e5b778581d428a50593d91c5f_e98b7563d8" tabindex="-1" value=""></div>
                                <div class="clear">
                                    <input type="submit" value="Enviar" name="subscribe" id="mc-embedded-subscribe" class="button">
                                </div>
                            </form>
                        </div> <!-- /.subscribe-form -->
                        <!-- Icons -->
                        <ul class="actions">
                            <li><a href="https://www.facebook.com/tavernadelasal/" target="_blank" title="Facebook" class="icon ion-social-facebook"><span class="label">Facebook</span></a></li>
                            <li><a href="https://www.instagram.com/tavernadelasal/" target="_blank" title="Instagram" class="icon ion-social-instagram"><span class="label">Instagram</span></a></li>
                        </ul>
                    </div>
                </div> <!-- /.row -->
                
                <div class="row logos-row">
                    <!--<ul class="logos">
                        <li><img src="assets/images/logo-petits-grans-hotels.png" alt="Costa Brava Hotels" width="109" height="60">
                        </li>
                        <li><img src="assets/images/logo-natura-i-turisme-actiu.png" alt="Costa Brava Pirineu de Girona" width="227" height="60">
                        </li>
                        <li><img src="assets/images/logo-bed-and-bike.png" alt="Bike & Bed" width="78" height="60"></li>
                    </ul>-->
                </div> <!-- /.row -->

            </div> <!-- /.contact-footer -->
        </div> <!-- /.container -->
    </aside>


    <!-- Footer -->
    <footer id="footer" class="">

        <!-- Menu -->
        <ul class="menu">
            <li>&copy; Taverna de la Sal</li>
            <!--<li>HG-000000</li>-->
            <li><a href="popups/legal-es.html" class="legal" title="Aviso legal" rel="modal:open">Avíso Legal</a></li>
            <li>Design: <a href="http://sokvist.com" title="Sokvist, Web & SEO">Sokvist</a></li>
        </ul>
        
        <a id="back-to-top" href="#one" title="Back to top"><span>To top</span></a>

    </footer>
    


    <script src="assets/js/vendor/jquery-1.11.2.min.js"></script>
    <script src="assets/js/plugins.js"></script>
    <script src="assets/js/js/main.min.js"></script>

    <script>
        /*(function(i,s,o,g,r,a,m){i['GoogleAnalyticsObject']=r;i[r]=i[r]||function(){
        (i[r].q=i[r].q||[]).push(arguments)},i[r].l=1*new Date();a=s.createElement(o),
        m=s.getElementsByTagName(o)[0];a.async=1;a.src=g;m.parentNode.insertBefore(a,m)
        })(window,document,'script','//www.google-analytics.com/analytics.js','ga');

        ga('create', 'UA-47544981-1', 'sokvist.com');
        ga('send', 'pageview');*/
    </script>
    
    <script src="//widget.siteminder.com/ibe.min.js"></script>

</body>
</html>