<!DOCTYPE html>
<!--[if lt IE 7 ]> <html class="ie ie6 ie-lt10 ie-lt9 ie-lt8 ie-lt7 no-js" lang="fr"> <![endif]-->
<!--[if IE 7 ]>    <html class="ie ie7 ie-lt10 ie-lt9 ie-lt8 no-js" lang="fr"> <![endif]-->
<!--[if IE 8 ]>    <html class="ie ie8 ie-lt10 ie-lt9 no-js" lang="fr"> <![endif]-->
<!--[if IE 9 ]>    <html class="ie ie9 ie-lt10 no-js" lang="fr"> <![endif]-->
<!--[if gt IE 9]><!--><html class="no-js" lang="fr"><!--<![endif]-->
<head>
    <!-- Global site tag (gtag.js) - Google Analytics -->
    <script async src="https://www.googletagmanager.com/gtag/js?id=UA-47544981-19"></script>
    <script>
      window.dataLayer = window.dataLayer || [];
      function gtag(){dataLayer.push(arguments);}
      gtag('js', new Date());

      gtag('config', 'UA-47544981-19');
    </script>
    
    <meta charset="utf-8">
	<title>Taverna de la Sal · L'Escala</title>
    <meta http-equiv="X-UA-Compatible" content="IE=edge" />
	<meta name="description" content="Hôtel de charme dans la Costa Brava, situé au centre historique de L’Escala, région de l’Empordà, à quelques pas de la Platja de les Barques, l'ancien port de pêche." />
    <meta name="keywords" content="hôtel, Costa Brava, Empordà, plages" />
    <meta name="author" content="sokvist.com">

    <meta name="viewport" content="width=device-width, initial-scale=1">

    <link rel="shortcut icon" href="favicon.ico" />
    <link rel="apple-touch-icon" href="apple-touch-icon.png" />
    <link rel="apple-touch-icon-precomposed" href="apple-touch-icon-precomposed.png">
    <link href="https://fonts.googleapis.com/css?family=Roboto:300,700" rel="stylesheet">
	<link rel="stylesheet" type="text/css" href="assets/css/style.css">
    
    <link rel="canonical" href="http://tavernadelasal.com/fr">
    <link hreflang="ca" href="http://tavernadelasal.com/" rel="alternate">
    <link hreflang="es" href="http://tavernadelasal.com/es/" rel="alternate">
    <link hreflang="en" href="http://tavernadelasal.com/en/" rel="alternate">
    
    <!-- Facebook Open Graph -->
    <meta property="og:type" content="website">
    <meta property="og:url" content="http://tavernadelasal.com/">
    <meta property="og:title" content="Taverna de la Sal · L'Escala">
    <meta property="og:image" content="assets/images/og-image.jpg">
    <meta property="og:description" content="Hôtel de charme dans la Costa Brava, situé au centre historique de L’Escala, région de l’Empordà, à quelques pas de la Platja de les Barques, l'ancien port de pêche.">
    <meta property="og:site_name" content="Taverna de la Sal · L'Escala">
    <meta property="og:locale" content="fr">
    <meta property="og:image:width" content="1200">
    <meta property="og:image:height" content="630">
    
    <!-- Twitter Card 
    <meta name="twitter:card" content="summary">
    <meta name="twitter:site" content="@expoprimats">
    <meta name="twitter:creator" content="@SokvistWeb">
    <meta name="twitter:url" content="http://tavernadelasal.com/">
    <meta name="twitter:title" content="Taverna de la Sal · L'Escala">
    <meta name="twitter:description" content="Hôtel de charme dans la Costa Brava, situé au centre historique de L’Escala, région de l’Empordà, à quelques pas de la Platja de les Barques, l'ancien port de pêche.">
    <meta name="twitter:image" content="assets/images/og-image.jpg">-->
    
    <script async src="assets/js/vendor/modernizr.js"></script>
</head>
    
    
<body class="home" data-region="emea" data-channelcode="tavernadelasaldirect">

    <!-- Header -->
    <header id="header">

        <!-- Logo -->
        <?php include 'includes/logo.php';?>
        
        <!-- Nav -->
        <nav class="main-nav smart-nav icon ion-ios-more" role="navigation">
            <div>
            <ul id="nav">
                <li><a href="#hotel">Hôtel</a></li>
                <li><a href="#habitacions">Chambres</a></li>
                <li><a href="#serveis">Services</a></li>
                <li><a href="#galeria">Galerie</a></li>
                <li><a href="#contacte">Contact</a></li>
            </ul>
            </div>
        </nav>
        
        <div class="contact">
            <a href="tel:0034972776278" class="icon ion-iphone"><span> +34 972 776 278</span></a>
            <a href="mailto:info@tavernadelasal.com" class="icon ion-ios-email"><span class="hidden">info@tavernadelasal.com</span></a>
            <a href="https://www.google.es/maps/place/Carrer+de+Santa+M%C3%A0xima,+7,+17130+L'Escala,+Girona/@42.1259022,3.1312329,17z/data=!3m1!4b1!4m5!3m4!1s0x12ba5f26727f0895:0x3f6c33fc81532504!8m2!3d42.1258982!4d3.1334216" class="icon ion-ios-location" title="Veure a Google maps" target="_blank"><span class="hidden">On som</span></a>
            <a href="#" class="lang-switcher icon ion-earth" title="Idioma"><span class="hidden">Idioma</span></a>
        </div>
        
        <div class="language">
            <ul class="qtranxs_language_chooser" id="qtranslate-chooser">
                <li class="lang-ca"><a href="ca" hreflang="ca" title="Català"><span>CA</span></a></li>
                <li class="lang-es"><a href="es" hreflang="es" title="Español"><span>ES</span></a></li>
                <li class="lang-en"><a href="en" hreflang="en" title="English"><span>EN</span></a></li>
                <li class="lang-fr active-lang"><a href="fr" hreflang="fr" title="Français"><span>FR</span></a></li>
            </ul>
            <div class="qtranxs_widget_end"></div>
        </div>
        
    </header>
    
    
    <div class="book-online">
        <h2>Réservation Online</h2>
        
        <!--<div class="getmore modal__trigger"><i class="icon ion-android-done"></i><span>Avantatges</span></div>-->
        
        <form action="#" class="ibe" method="get" data-region="emea" data-channelcode="tavernadelasaldirect" data-widget="property_check_availability" target="_blank">
            <fieldset>
                <div class="field">
                    Arrivée:
                    <input name="check_in_date" class="datepicker-input" data-language="es" data-position="top right" data-date-format="dd/mm/yyyy" id="check_in_date" value="" type="text" data-init-val="null">
                    Départ:
                    <input name="check_out_date" class="datepicker-input" data-language="es" data-position="top right" data-date-format="dd/mm/yyyy" id="check_out_date" value="" type="text" data-init-val="null">
                    <input name="commit" value="Réserver" type="submit">
                    
                    <a class="offers" href="popups/offer-fr.html" title="Avantatges" rel="modal:open">Avantages</a>
                </div>
            </fieldset>
        </form>

    </div><!-- /.book-online -->
    
    <!-- Mobile booking button -->
    <a href="#" class="booking-btn ibe" data-region="emea" data-channelcode="tavernadelasaldirect" target="_blank">Réservation Online</a>
    

    <div id="btm-frame">
        <h2>Boutique Hotel Only Adults</h2>
        <nav>
            <ul id="arrow-nav" class="arrow-nav">
                <li><a href="#one" class="icon ion-android-arrow-down down bounce" id="one-btn">More</a></li>
                <li><a href="#two" class="icon ion-android-arrow-down down" id="two-btn">Next</a></li>
            </ul>
        </nav>
    </div>
    

    <div id="lft-frame"></div>
    <div id="rgt-frame"></div>
        

    <!-- Intro -->
    <section id="intro" class="fixed">
        <div class="bg-image-bn"></div>
        <div class="bg-image-color"></div>
    </section>

    <!-- One -->
    <section id="one">
    </section>
        
        
    <div class="modernist"></div>


    <!-- two -->
    <section id="hotel" class="two main style3" data-animate-down="arrow-hide" data-animate-up="arrow-show">
        <div class="container container-narrow">
            <div class="row">
                <div class="column fullwidth">
                    <header>
                        <h1>Taverna de la Sal</h1>
                    </header>
                    <div class="copy">
                        <h2>Taverna de la Sal est bel et bien l'hôtel de charme que vous recherchez dans la Costa Brava</h2>
                        
                        <p>Hôtel de charme dans la région de l’Empordà, situé au centre historique de l’Escala, à 20 mètres de la Platja de les Barques, l'ancien port de pêche.</p>
                        
                    </div>
                </div>
            </div> <!-- /.row -->
            
            <div class="row">
                <div class="column">
                    
                    <?php include 'includes/home-slider.php';?>
                    
                </div>
            </div> <!-- /.row -->
            
            <div class="row">
                <div class="column fullwidth">
                    <div class="copy copy-btm">
                        
                        <p>Au beau milieu de plages et de paysages naturels uniques, vous pourrez profiter d'un séjour dans ce petit hôtel exclusivement réservé aux adultes, où notre principal engagement est votre bien-être.</p>
                        
                        <p>Hôtel de charme sur la Costa Brava- Empordà -l'Escala, composé de 6 belles chambres au style méditerranéen, avec une décoration soignée jusqu'au moindre détail. Un coin idéal pour s'évader au cœur de la Costa Brava...</p>
                        
                    </div>
                </div>
            </div> <!-- /.row -->
        </div> <!-- /.container -->
    </section>


    <!-- three -->
    <section id="habitacions" class="three main style3">
        <div class="modernist"></div>
        <div class="container">
            
            <h2>Chambres</h2>
            
            
            <div class="slider" id="basica">
                
                <?php include 'includes/rooms-slider.php';?>
                
                <div class="copy">
                    
                    <h3>Chambres spacieuses et confortables. Repos, confort et bonnes sensations</h3>
                    
                    <p>Taverna de la Sal est un charmant hôtel romantique en bord de mer doté de 6 chambres doubles, chacune ayant sa propre personnalité et étant soignée dans les moindres détails. Certaines chambres sont convertibles en chambres triples. Toutes les chambres sont réservées aux adultes de 14 ans et plus qui souhaitent séjourner dans un endroit tranquille, à quelques pas de la mer, et en plein centre historique de l'Escala.</p>
                    <p>Chambres spacieuses avec un lit double. Toutes les chambres disposent d'un balcon avec vue latérale sur la mer et sont équipées du suivant :</p>
                    
                    <ul class="custom-li">
                        <li>Lit double 2,00 x 1,80 m</li>
                        <li>Climatisation et chauffage</li>
                        <li>Salle de bain complète avec douche</li>
                        <li>Téléviseur à écran plat (chaînes internationales)</li>
                        <li>Coffre-fort</li>
                        <li>Mini-réfrigérateur</li>
                        <li>Sèche-cheveux</li>
                        <li>Bouilloire pour le café et le thé</li>
                        <li>Internet gratuit via Wi-Fi</li>
                        <li>Commodités de salle de bain</li>
                    </ul>
                    
                    <p>Chambres complètement non-fumeurs.</p>
                    
                    <p>Les animaux domestiques ne sont pas admis.</p>
                    
                    <div class="buttons">
                        <a class="booknow ibe" href="#" data-region="emea" data-channelcode="tavernadelasaldirect" target="_blank" title="Online booking page">Réserver</a>
                        <a class="offers offers-rooms" href="popups/offer-fr.html" title="Advantage" rel="modal:open">Avantages</a>
                    </div>

                </div>
            </div> <!-- .slider -->
            
            
        </div> <!-- /.container -->
    </section>


    <!-- four -->
    <section id="serveis" class="four main style3">
        <div class="modernist"></div>
        <div class="container">
            <div class="row">
                <div class="column">
                    <header>
                        <h2>Services</h2>
                    </header>

                    <div class="copy">
                        
                        <h3>Nous vous offrons tout ce dont vous avez besoin, car nous souhaitons que votre séjour à l'Escala soit unique</h3>
                        
                        <h4>Restaurant Taverna de la Sal</h4>
                        <p>Le restaurant est situé au rez-de-chaussée et au premier étage du bâtiment. Bien que nous nous spécialisons dans les viandes grillées, nous vous proposons également un large éventail de plats inspirés de la cuisine traditionnelle catalane. De plus, nous travaillons exclusivement avec des produits frais et locaux.</p>
                        
                        <h4>La Terrassa</h4>
                        <p>La Terrassa est situé sur le toit de Taverna de la Sal. Il s'agit d'un solarium prévu pour ceux qui préfèrent prendre un bain de soleil dans l'hôtel, ou tout simplement profiter des magnifiques couchers du soleil qui vous laisseront bouche bée!</p>
                        
                        <h4>Petit-déjeuner</h4>
                        <p>Le petit-déjeuner est servi sous forme de buffet de 8h 00 à 10h 30.</p>
                        
                        <h4>À votre service</h4>
                        <p>Nous parlons plusieurs langues et nous nous adaptons à chaque demande spécifique de notre clientèle, toujours avec le sourire. Nous nous consacrons à faire de votre séjour, l’un des meilleurs.</p>

                        <p>Nous serons toujours ravis de partager notre connaissance sur les endroits à visiter, les plus belles vues et les événements culturels en fonction de vos intérêts.</p>
                        
                        <h4>Bar-cafétéria</h4>
                        <p>Le bar-cafétéria situé au sein même du restaurant restera ouvert toute la journée pour les clients qui le souhaitent. Nous disposons d'un menu de boissons et de cocktails que vous pouvez apprécier quand vous voulez.</p>
                        
                        <h4>Parking</h4>
                        <p>Veuillez noter que nous sommes dans une zone piétonne uniquement. Nous vous offrons un parking sécurisé et couvert à 250m approximativement de l’hôtel. Il faut cependant réserver à l’avance.</p>

                        <p>Le prix est de 15€ par nuit.</p>

                        <p>Écrivez à la section COMMENTAIRES de votre réservation l’intérêt de réserver une place pour votre voiture dans notre garage sécurisé. Si nos places de parking sont complètes, nous pouvons vous informer sur d’autres options disponibles dans les environs. </p>
                        
                    </div>
                    
                </div>
            </div> <!-- /.row -->
        </div> <!-- /.container -->
    </section>
    
    
    <!-- five -->
    <section id="galeria" class="three main style3">
        <div class="modernist"></div>
        <div class="container">
                    
            <h2>Galerie</h2>

            <div class="copy">
                <p>L'hôtel de charme Taverna de la Sal de la Costa Brava est situé dans un milieu privilégié pour les amoureux de la mer et de la nature. En général, la région de l'Empordà, et particulièrement l'Escala, combinent à la perfection leurs belles plages avec un tourisme actif, des sports nautiques, une cuisine de haute qualité, sans oublier la culture et la tradition.</p>
            </div>
            
            <?php include 'includes/gallery.php';?>
            
        </div>
    </section>


    <!-- Contact -->
    <aside id="contacte" class="contacte main style3">
        <div class="modernist modernist-light"></div>
        <div class="container">
            
            <h2>Contact</h2>
            
            <div class="contact-footer">

                <div class="row">
                    <div class="column lft-column">
                        <h3>Taverna de la Sal</h3>
                        <p>Santa Màxima, 7<br>
                        L'Escala,<br>
                        17130 Girona<br>
                        Tel:<a href="tel:0034972776278"> 0034 972 776 278</a><br>
                        <a href="mailto:info@tavernadelasal.com"><span>info@tavernadelasal.com</span></a>
                        </p>
                    </div>
                    <div class="column ctr-column">
                        <!-- Nav Footer -->
                        <nav class="footer-nav" role="navigation">
                            <ul>
                                <li><a href="#hotel">Hôtel</a></li>
                                <li><a href="#habitacions">Chambres</a></li>
                                <li><a href="#serveis">Services</a></li>
                                <li><a href="#galeria">Galerie</a></li>
                                <li><a href="#contacte">Contact</a></li>
                            </ul>
                        </nav>
                    </div>
                    <div class="column rgt-column">
                        <!-- Mailchimp Subscribe Form -->
                        <div id="mc_embed_signup" class="subscribe-form">
                            <h3>Abonnez-vous et nous vous informerons de nos offres et nouvelles</h3>
                            <form action="" method="post" id="mc-embedded-subscribe-form" name="mc-embedded-subscribe-form" class="validate" target="_blank" novalidate="">

                                <div class="mc-field-group">
                                    <input type="email" value="" name="EMAIL" class="required email" id="mce-EMAIL" placeholder="Email">
                                </div>

                                <input type="text" value="" name="FNAME" class="" id="mce-FNAME" placeholder="Nom">
                                <div id="mce-responses" class="clear">
                                    <div class="response" id="mce-error-response"></div>
                                    <div class="response" id="mce-success-response"></div>
                                </div>    <!-- real people should not fill this in and expect good things - do not remove this or risk form bot signups-->
                                <div id="real-people" aria-hidden="true"><input type="text" name="b_e5b778581d428a50593d91c5f_e98b7563d8" tabindex="-1" value=""></div>
                                <div class="clear">
                                    <input type="submit" value="Envoyer" name="subscribe" id="mc-embedded-subscribe" class="button">
                                </div>
                            </form>
                        </div> <!-- /.subscribe-form -->
                        <!-- Icons -->
                        <ul class="actions">
                            <li><a href="https://www.facebook.com/tavernadelasal/" target="_blank" title="Facebook" class="icon ion-social-facebook"><span class="label">Facebook</span></a></li>
                            <li><a href="https://www.instagram.com/tavernadelasal/" target="_blank" title="Instagram" class="icon ion-social-instagram"><span class="label">Instagram</span></a></li>
                        </ul>
                    </div>
                </div> <!-- /.row -->
                
                <div class="row logos-row">
                    <!--<ul class="logos">
                        <li><img src="assets/images/logo-petits-grans-hotels.png" alt="Costa Brava Hotels" width="109" height="60">
                        </li>
                        <li><img src="assets/images/logo-natura-i-turisme-actiu.png" alt="Costa Brava Pirineu de Girona" width="227" height="60">
                        </li>
                        <li><img src="assets/images/logo-bed-and-bike.png" alt="Bike & Bed" width="78" height="60"></li>
                    </ul>-->
                </div> <!-- /.row -->

            </div> <!-- /.contact-footer -->
        </div> <!-- /.container -->
    </aside>


    <!-- Footer -->
    <footer id="footer" class="">

        <!-- Menu -->
        <ul class="menu">
            <li>&copy; Taverna de la Sal</li>
            <!--<li>HG-000000</li>-->
            <li><a href="popups/legal-en.html" class="legal" title="Legal Notice" rel="modal:open">Legal Notice</a></li>
            <li>Design: <a href="http://sokvist.com" title="Sokvist, Web & SEO">Sokvist</a></li>
        </ul>
        
        <a id="back-to-top" href="#one" title="Back to top"><span>To top</span></a>

    </footer>
    


    <script src="assets/js/vendor/jquery-1.11.2.min.js"></script>
    <script src="assets/js/plugins.js"></script>
    <script src="assets/js/js/main.min.js"></script>

    <script>
        /*(function(i,s,o,g,r,a,m){i['GoogleAnalyticsObject']=r;i[r]=i[r]||function(){
        (i[r].q=i[r].q||[]).push(arguments)},i[r].l=1*new Date();a=s.createElement(o),
        m=s.getElementsByTagName(o)[0];a.async=1;a.src=g;m.parentNode.insertBefore(a,m)
        })(window,document,'script','//www.google-analytics.com/analytics.js','ga');

        ga('create', 'UA-47544981-1', 'sokvist.com');
        ga('send', 'pageview');*/
    </script>
    
    <script src="//widget.siteminder.com/ibe.min.js"></script>

</body>
</html>