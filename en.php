<!DOCTYPE html>
<!--[if lt IE 7 ]> <html class="ie ie6 ie-lt10 ie-lt9 ie-lt8 ie-lt7 no-js" lang="en"> <![endif]-->
<!--[if IE 7 ]>    <html class="ie ie7 ie-lt10 ie-lt9 ie-lt8 no-js" lang="en"> <![endif]-->
<!--[if IE 8 ]>    <html class="ie ie8 ie-lt10 ie-lt9 no-js" lang="en"> <![endif]-->
<!--[if IE 9 ]>    <html class="ie ie9 ie-lt10 no-js" lang="en"> <![endif]-->
<!--[if gt IE 9]><!--><html class="no-js" lang="en"><!--<![endif]-->
<head>
    <!-- Global site tag (gtag.js) - Google Analytics -->
    <script async src="https://www.googletagmanager.com/gtag/js?id=UA-47544981-19"></script>
    <script>
      window.dataLayer = window.dataLayer || [];
      function gtag(){dataLayer.push(arguments);}
      gtag('js', new Date());

      gtag('config', 'UA-47544981-19');
    </script>
    
    <meta charset="utf-8">
	<title>Taverna de la Sal · L'Escala</title>
    <meta http-equiv="X-UA-Compatible" content="IE=edge" />
	<meta name="description" content="Our small and romantic hotel is situated 20m from the beach in the authentic old town of L’Escala, in the Costa Brava" />
    <meta name="keywords" content="hotel, Costa Brava, Empordà, beach" />
    <meta name="author" content="sokvist.com">

    <meta name="viewport" content="width=device-width, initial-scale=1">

    <link rel="shortcut icon" href="favicon.ico" />
    <link rel="apple-touch-icon" href="apple-touch-icon.png" />
    <link rel="apple-touch-icon-precomposed" href="apple-touch-icon-precomposed.png">
    <link href="https://fonts.googleapis.com/css?family=Roboto:300,700" rel="stylesheet">
	<link rel="stylesheet" type="text/css" href="assets/css/style.css">
    
    <link rel="canonical" href="http://tavernadelasal.com/en">
    <link hreflang="es" href="http://tavernadelasal.com/es/" rel="alternate">
    <link hreflang="ca" href="http://tavernadelasal.com/" rel="alternate">
    <link hreflang="fr" href="http://tavernadelasal.com/fr/" rel="alternate">
    
    <!-- Facebook Open Graph -->
    <meta property="og:type" content="website">
    <meta property="og:url" content="http://tavernadelasal.com/">
    <meta property="og:title" content="Taverna de la Sal · L'Escala">
    <meta property="og:image" content="assets/images/og-image.jpg">
    <meta property="og:description" content="Our small and romantic hotel is situated 20m from the beach in the authentic old town of L’Escala, in the Costa Brava">
    <meta property="og:site_name" content="Taverna de la Sal · L'Escala">
    <meta property="og:locale" content="en">
    <meta property="og:image:width" content="1200">
    <meta property="og:image:height" content="630">
    
    <!-- Twitter Card 
    <meta name="twitter:card" content="summary">
    <meta name="twitter:site" content="@expoprimats">
    <meta name="twitter:creator" content="@SokvistWeb">
    <meta name="twitter:url" content="http://tavernadelasal.com/">
    <meta name="twitter:title" content="Taverna de la Sal · L'Escala">
    <meta name="twitter:description" content="Our small and romantic hotel is situated 20m from the beach in the authentic old town of L’Escala, in the Costa Brava">
    <meta name="twitter:image" content="assets/images/og-image.jpg">-->
    
    <script async src="assets/js/vendor/modernizr.js"></script>
</head>
    
    
<body class="home" data-region="emea" data-channelcode="tavernadelasaldirect">

    <!-- Header -->
    <header id="header">

        <!-- Logo -->
        <?php include 'includes/logo.php';?>
        
        <!-- Nav -->
        <nav class="main-nav smart-nav icon ion-ios-more" role="navigation">
            <div>
            <ul id="nav">
                <li><a href="#hotel">Hotel</a></li>
                <li><a href="#habitacions">Bedrooms</a></li>
                <li><a href="#serveis">Services</a></li>
                <li><a href="#galeria">Gallery</a></li>
                <li><a href="#contacte">Contact</a></li>
            </ul>
            </div>
        </nav>
        
        <div class="contact">
            <a href="tel:0034972776278" class="icon ion-iphone"><span> +34 972 776 278</span></a>
            <a href="mailto:info@tavernadelasal.com" class="icon ion-ios-email"><span class="hidden">info@tavernadelasal.com</span></a>
            <a href="https://www.google.es/maps/place/Carrer+de+Santa+M%C3%A0xima,+7,+17130+L'Escala,+Girona/@42.1259022,3.1312329,17z/data=!3m1!4b1!4m5!3m4!1s0x12ba5f26727f0895:0x3f6c33fc81532504!8m2!3d42.1258982!4d3.1334216" class="icon ion-ios-location" title="Veure a Google maps" target="_blank"><span class="hidden">On som</span></a>
            <a href="#" class="lang-switcher icon ion-earth" title="Idioma"><span class="hidden">Idioma</span></a>
        </div>
        
        <div class="language">
            <ul class="qtranxs_language_chooser" id="qtranslate-chooser">
                <li class="lang-ca"><a href="ca" hreflang="ca" title="Català"><span>CA</span></a></li>
                <li class="lang-es"><a href="es" hreflang="es" title="Español"><span>ES</span></a></li>
                <li class="lang-en active-lang"><a href="en" hreflang="en" title="English"><span>EN</span></a></li>
                <li class="lang-fr"><a href="fr" hreflang="fr" title="Français"><span>FR</span></a></li>
            </ul>
            <div class="qtranxs_widget_end"></div>
        </div>
        
    </header>
    
    
    <div class="book-online">
        <h2>Online booking</h2>
        
        <!--<div class="getmore modal__trigger"><i class="icon ion-android-done"></i><span>Avantatges</span></div>-->
        
        <form action="#" class="ibe" method="get" data-region="emea" data-channelcode="tavernadelasaldirect" data-widget="property_check_availability" target="_blank">
            <fieldset>
                <div class="field">
                    Check-in:
                    <input name="check_in_date" class="datepicker-input" data-language="es" data-position="top right" data-date-format="dd/mm/yyyy" id="check_in_date" value="" type="text" data-init-val="null">
                    Check-out:
                    <input name="check_out_date" class="datepicker-input" data-language="es" data-position="top right" data-date-format="dd/mm/yyyy" id="check_out_date" value="" type="text" data-init-val="null">
                    <input name="commit" value="Book" type="submit">
                    
                    <a class="offers" href="popups/offer-en.html" title="Avantatge" rel="modal:open">Advantage</a>
                </div>
            </fieldset>
        </form>

    </div><!-- /.book-online -->
    
    <!-- Mobile booking button -->
    <a href="#" class="booking-btn ibe" data-region="emea" data-channelcode="tavernadelasaldirect" target="_blank">Online booking</a>
    
    <a href="popups/offer-en.html" class="advantage-btn" title="Avantatge" rel="modal:open">Advantage</a>
    

    <div id="btm-frame">
        <h2>Boutique Hotel Only Adults</h2>
        <nav>
            <ul id="arrow-nav" class="arrow-nav">
                <li><a href="#one" class="icon ion-android-arrow-down down bounce" id="one-btn">More</a></li>
                <li><a href="#two" class="icon ion-android-arrow-down down" id="two-btn">Next</a></li>
            </ul>
        </nav>
    </div>
    

    <div id="lft-frame"></div>
    <div id="rgt-frame"></div>
        

    <!-- Intro -->
    <section id="intro" class="fixed">
        <div class="bg-image-bn"></div>
        <div class="bg-image-color"></div>
    </section>

    <!-- One -->
    <section id="one">
    </section>
        
        
    <div class="modernist"></div>


    <!-- two -->
    <section id="hotel" class="two main style3" data-animate-down="arrow-hide" data-animate-up="arrow-show">
        <div class="container container-narrow">
            <div class="row">
                <div class="column fullwidth">
                    <header>
                        <h1>Taverna de la Sal</h1>
                    </header>
                    <div class="copy">
                        <h2>Taverna de la Sal is the charming little hotel on the Costa Brava sea front you have been looking for</h2>
                        
                        <p>Our small and romantic hotel is situated 20m from the beach in the authentic old town of l’Escala. “Platja de les barques” is named after the fishing boats that used to pull up on the beach to unload their anchovies direct to the daily auction.</p>
                        
                        <p>Flanked by magnificent wide open sandy beaches on one side and small rocky bays on the other, a selection of natural areas ideal for nature walks and bicycles, and marine parks our small hotel competes as one of the Costa Bravas best beach hotels. </p>
                        
                    </div>
                </div>
            </div> <!-- /.row -->
            
            <div class="row">
                <div class="column">
                    
                    <?php include 'includes/home-slider.php';?>
                    
                </div>
            </div> <!-- /.row -->
            
            <div class="row">
                <div class="column fullwidth">
                    <div class="copy copy-btm">
                        
                        <p>In order to maintain our small boutique hotel atmosphere, we accept only adults and belong to the Costa Brava only adults hotel group.</p>
                        
                        <p>Being a hotel on the beach (20m) all six well equipped rooms have their own balcony with uninterrupted sea and beach views all day long. The hotel is housed in an old stone building (1850) and the bedrooms are sensitively decorated in a modern Mediterranean style making us your small luxury hotel on the Costa Brava.</p>
                        
                    </div>
                </div>
            </div> <!-- /.row -->
        </div> <!-- /.container -->
    </section>


    <!-- three -->
    <section id="habitacions" class="three main style3">
        <div class="modernist"></div>
        <div class="container">
            
            <h2>Bedrooms</h2>
            
            
            <div class="slider" id="basica">
                
                <?php include 'includes/rooms-slider.php';?>
                
                <div class="copy">
                    <h3>Spacious and comfortable rooms. Rest, comfort and good sensations</h3>
                    
                    <p>Enjoy being right in the heart of all the l’Escala has to offer without even leaving your bedroom. All 6 of our spacious and welcoming rooms make it easy for you to unwind and relax in comfort.</p>
                    
                    <p>We aim to make your bedroom your home away from home. All bedrooms are for adults only (14 years or older) in order to maintain our relaxed and select atmosphere.</p>
                    
                    <p>All rooms accomodate double beds and are capable of adding a third bed if needed:</p>
                    
                    <ul class="custom-li">
                    <li>Double bed 2,00m x 1,80m</li>
                    <li>Air conditioning and heating</li>
                    <li>En suite bathrooms with large walk in shower</li>
                    <li>Flat screen TV with International channels</li>
                    <li>Mini safe</li>
                    <li>Mini fridge</li>
                    <li>Hair dryer</li>
                    <li>Kettle for tea and/or coffee</li>
                    <li>Free WiFi</li>
                    <li>Bathroom complementaries</li>
                    </ul>
                    
                    <p>All bedrooms are 100% smoke free.</p>
                    
                    <p>We don’t accept pets.</p>

                    <div class="buttons">
                        <a class="booknow ibe" href="#" data-region="emea" data-channelcode="tavernadelasaldirect" target="_blank" title="Online booking page">Book now</a>
                        <a class="offers offers-rooms" href="popups/offer-en.html" title="Advantage" rel="modal:open">Advantage</a>
                    </div>

                </div>
            </div> <!-- .slider -->
            
            
        </div> <!-- /.container -->
    </section>


    <!-- four -->
    <section id="serveis" class="four main style3">
        <div class="modernist"></div>
        <div class="container">
            <div class="row">
                <div class="column">
                    <header>
                        <h2>Services</h2>
                    </header>

                    <div class="copy">
                        
                        <h3>We offer all you will need to make your stay in l’Escala unique</h3>
                        
                        <h4>Restaurant Taverna de la Sal</h4>
                        <p>The restaurant specialises in barbeque meats and fish and is situated on the ground and first floors of the building. We also offer a good selection of tradicional Catalan and some International dishes prepared with fresh local produce.</p>
                        
                        <h4>The Terrace</h4>
                        <p>Our rooftop terrace is a great place to sunbathe for those who prefer to stay in the hotel, or simply admire the sun setting over the sea.</p>
                        
                        <h4>Breakfast</h4>
                        <p>We serve a buffet style breakfast from 8am until 10,30am in the first floor restaurant.</p>
                        
                        <h4>Service</h4>
                        <p>We speak several languages and can adapt to each customers special requests – always with a smile. We are dedicated to making your stay the memorable occasion you hope it to be.</p>

                        <p>We are always happy to share our local knowledge of places to visit, special sights and cultural events depending on your interests. </p>
                        
                        <h4>Bar – Cafeteria</h4>
                        <p>During the summer months our bar located on the ground floor is open all day. We offer a range of cocktails, juices, craft beers and local wines.</p>
                        
                        <h4>Parking</h4>
                        <p>Please note we are located in a pedestrian only area. Approx 250m from the hotel we offer covered and secure parking. If desired, it is necessary to reserve this limited service in advance.</p>

                        <p>The price is 19,00€ per night.</p>

                        <p>Write in the COMMENTS area on your booking about your interest of having a space in our secure garage to park your car. If our parking places are full, we can inform you about other options near by.</p>
                        
                    </div>
                    
                </div>
            </div> <!-- /.row -->
        </div> <!-- /.container -->
    </section>
    
    
    <!-- five -->
    <section id="galeria" class="three main style3">
        <div class="modernist"></div>
        <div class="container">
                    
            <h2>Gallery</h2>

            <div class="copy">
                <p>L’Escala, Emporda, Costa brava. An area unique for it’s diversity of beaches, natural beauty and strong culture make us an ideal destination to practice land and/or sea based sports, dive into local gastronomy or immerse in some local traditions.</p>
            </div>
            
            <?php include 'includes/gallery.php';?>
            
        </div>
    </section>


    <!-- Contact -->
    <aside id="contacte" class="contacte main style3">
        <div class="modernist modernist-light"></div>
        <div class="container">
            
            <h2>Contact</h2>
            
            <div class="contact-footer">

                <div class="row">
                    <div class="column lft-column">
                        <h3>Taverna de la Sal</h3>
                        <p>Santa Màxima, 7<br>
                        L'Escala,<br>
                        17130 Girona<br>
                        Tel:<a href="tel:0034972776278"> 0034 972 776 278</a><br>
                        <a href="mailto:info@tavernadelasal.com"><span>info@tavernadelasal.com</span></a>
                        </p>
                    </div>
                    <div class="column ctr-column">
                        <!-- Nav Footer -->
                        <nav class="footer-nav" role="navigation">
                            <ul>
                                <li><a href="#hotel">Hotel</a></li>
                                <li><a href="#habitacions">Bedrooms</a></li>
                                <li><a href="#serveis">Services</a></li>
                                <li><a href="#galeria">Gallery</a></li>
                                <li><a href="#contacte">Contact</a></li>
                            </ul>
                        </nav>
                    </div>
                    <div class="column rgt-column">
                        <!-- Mailchimp Subscribe Form -->
                        <div id="mc_embed_signup" class="subscribe-form">
                            <h3>Subscribe to receive our latest information</h3>
                                <form action="#" method="post" id="mc-embedded-subscribe-form" name="mc-embedded-subscribe-form" class="validate" target="_blank" novalidate="">

                                <div class="mc-field-group">
                                    <input type="email" value="" name="EMAIL" class="required email" id="mce-EMAIL" placeholder="Email">
                                </div>

                                <input type="text" value="" name="FNAME" class="" id="mce-FNAME" placeholder="Name">
                                <div id="mce-responses" class="clear">
                                    <div class="response" id="mce-error-response"></div>
                                    <div class="response" id="mce-success-response"></div>
                                </div>    <!-- real people should not fill this in and expect good things - do not remove this or risk form bot signups-->
                                <div id="real-people" aria-hidden="true"><input type="text" name="b_e5b778581d428a50593d91c5f_e98b7563d8" tabindex="-1" value=""></div>
                                <div class="clear">
                                    <input type="submit" value="Submit" name="subscribe" id="mc-embedded-subscribe" class="button">
                                </div>
                            </form>
                        </div> <!-- /.subscribe-form -->
                        <!-- Icons -->
                        <ul class="actions">
                            <li><a href="https://www.facebook.com/tavernadelasal/" target="_blank" title="Facebook" class="icon ion-social-facebook"><span class="label">Facebook</span></a></li>
                            <li><a href="https://www.instagram.com/tavernadelasal/" target="_blank" title="Instagram" class="icon ion-social-instagram"><span class="label">Instagram</span></a></li>
                        </ul>
                    </div>
                </div> <!-- /.row -->
                
                <div class="row logos-row">
                    <!--<ul class="logos">
                        <li><img src="assets/images/logo-petits-grans-hotels.png" alt="Costa Brava Hotels" width="109" height="60">
                        </li>
                        <li><img src="assets/images/logo-natura-i-turisme-actiu.png" alt="Costa Brava Pirineu de Girona" width="227" height="60">
                        </li>
                        <li><img src="assets/images/logo-bed-and-bike.png" alt="Bike & Bed" width="78" height="60"></li>
                    </ul>-->
                </div> <!-- /.row -->

            </div> <!-- /.contact-footer -->
        </div> <!-- /.container -->
    </aside>


    <!-- Footer -->
    <footer id="footer" class="">

        <!-- Menu -->
        <ul class="menu">
            <li>&copy; Taverna de la Sal</li>
            <!--<li>HG-000000</li>-->
            <li><a href="popups/legal-en.html" class="legal" title="Legal Notice" rel="modal:open">Legal Notice</a></li>
            <li>Design: <a href="http://sokvist.com" title="Sokvist, Web & SEO">Sokvist</a></li>
        </ul>
        
        <a id="back-to-top" href="#one" title="Back to top"><span>To top</span></a>

    </footer>
    


    <script src="assets/js/vendor/jquery-1.11.2.min.js"></script>
    <script src="assets/js/plugins.js"></script>
    <script src="assets/js/js/main.min.js"></script>

    <script>
        /*(function(i,s,o,g,r,a,m){i['GoogleAnalyticsObject']=r;i[r]=i[r]||function(){
        (i[r].q=i[r].q||[]).push(arguments)},i[r].l=1*new Date();a=s.createElement(o),
        m=s.getElementsByTagName(o)[0];a.async=1;a.src=g;m.parentNode.insertBefore(a,m)
        })(window,document,'script','//www.google-analytics.com/analytics.js','ga');

        ga('create', 'UA-47544981-1', 'sokvist.com');
        ga('send', 'pageview');*/
    </script>
    
    <script src="//widget.siteminder.com/ibe.min.js"></script>

</body>
</html>