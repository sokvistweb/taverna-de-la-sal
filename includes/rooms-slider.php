<ul class="rslides" id="slider1">
    <li><img src="assets/images/rooms/room-1.jpg" alt="Rooms - Hotel Taverna de la Sal, l'Escala" width="1080" height="562"></li>
    <li><img src="assets/images/rooms/room-2.jpg" alt="Rooms - Hotel Taverna de la Sal, l'Escala" width="1080" height="562"></li>
    <li><img src="assets/images/rooms/room-3.jpg" alt="Rooms - Hotel Taverna de la Sal, l'Escala" width="1080" height="562"></li>
    <li><img src="assets/images/rooms/room-4.jpg" alt="Rooms - Hotel Taverna de la Sal, l'Escala" width="1080" height="562"></li>
    <li><img src="assets/images/rooms/room-5.jpg" alt="Rooms - Hotel Taverna de la Sal, l'Escala" width="1080" height="562"></li>
    <li><img src="assets/images/rooms/room-6.jpg" alt="Rooms - Hotel Taverna de la Sal, l'Escala" width="1080" height="562"></li>
    <li><img src="assets/images/rooms/room-7.jpg" alt="Rooms - Hotel Taverna de la Sal, l'Escala" width="1080" height="562"></li>
    <li><img src="assets/images/rooms/room-8.jpg" alt="Rooms - Hotel Taverna de la Sal, l'Escala" width="1080" height="562"></li>
    <li><img src="assets/images/rooms/room-9.jpg" alt="Rooms - Hotel Taverna de la Sal, l'Escala" width="1080" height="562"></li>
    <li><img src="assets/images/rooms/room-10.jpg" alt="Rooms - Hotel Taverna de la Sal, l'Escala" width="1080" height="562"></li>
</ul>
<!-- Slideshow 3 Pager -->
<ul class="slider-pager" id="slider1-pager">
    <li><a href="#"><img src="assets/images/rooms/th-room-1.jpg" alt="Rooms - Hotel Taverna de la Sal, l'Escala" width="120" height="62"></a></li>
    <li><a href="#"><img src="assets/images/rooms/th-room-2.jpg" alt="Rooms - Hotel Taverna de la Sal, l'Escala" width="120" height="62"></a></li>
    <li><a href="#"><img src="assets/images/rooms/th-room-3.jpg" alt="Rooms - Hotel Taverna de la Sal, l'Escala" width="120" height="62"></a></li>
    <li><a href="#"><img src="assets/images/rooms/th-room-4.jpg" alt="Rooms - Hotel Taverna de la Sal, l'Escala" width="120" height="62"></a></li>
    <li><a href="#"><img src="assets/images/rooms/th-room-5.jpg" alt="Rooms - Hotel Taverna de la Sal, l'Escala" width="120" height="62"></a></li>
    <li><a href="#"><img src="assets/images/rooms/th-room-6.jpg" alt="Rooms - Hotel Taverna de la Sal, l'Escala" width="120" height="62"></a></li>
    <li><a href="#"><img src="assets/images/rooms/th-room-7.jpg" alt="Rooms - Hotel Taverna de la Sal, l'Escala" width="120" height="62"></a></li>
    <li><a href="#"><img src="assets/images/rooms/th-room-8.jpg" alt="Rooms - Hotel Taverna de la Sal, l'Escala" width="120" height="62"></a></li>
    <li><a href="#"><img src="assets/images/rooms/th-room-9.jpg" alt="Rooms - Hotel Taverna de la Sal, l'Escala" width="120" height="62"></a></li>
    <li><a href="#"><img src="assets/images/rooms/th-room-10.jpg" alt="Rooms - Hotel Taverna de la Sal, l'Escala" width="120" height="62"></a></li>
</ul>