<!DOCTYPE html>
<!--[if lt IE 7 ]> <html class="ie ie6 ie-lt10 ie-lt9 ie-lt8 ie-lt7 no-js" lang="ca"> <![endif]-->
<!--[if IE 7 ]>    <html class="ie ie7 ie-lt10 ie-lt9 ie-lt8 no-js" lang="ca"> <![endif]-->
<!--[if IE 8 ]>    <html class="ie ie8 ie-lt10 ie-lt9 no-js" lang="ca"> <![endif]-->
<!--[if IE 9 ]>    <html class="ie ie9 ie-lt10 no-js" lang="ca"> <![endif]-->
<!--[if gt IE 9]><!--><html class="no-js" lang="ca"><!--<![endif]-->
<head>
    <!-- Global site tag (gtag.js) - Google Analytics -->
    <script async src="https://www.googletagmanager.com/gtag/js?id=UA-47544981-19"></script>
    <script>
      window.dataLayer = window.dataLayer || [];
      function gtag(){dataLayer.push(arguments);}
      gtag('js', new Date());

      gtag('config', 'UA-47544981-19');
    </script>
    
    <meta charset="utf-8">
	<title>Taverna de la Sal · L'Escala</title>
    <meta http-equiv="X-UA-Compatible" content="IE=edge" />
	<meta name="description" content="Hotel amb encant a la Costa Brava, situat al casc antic del municipi de l’Escala, Alt Empordà, a pocs metres de la Platja de les Barques, antic port de pescadors." />
    <meta name="keywords" content="hotel, Costa Brava, Empordà, platja" />
    <meta name="author" content="sokvist.com">

    <meta name="viewport" content="width=device-width, initial-scale=1">

    <link rel="shortcut icon" href="favicon.ico" />
    <link rel="apple-touch-icon" href="apple-touch-icon.png" />
    <link rel="apple-touch-icon-precomposed" href="apple-touch-icon-precomposed.png">
    <link href="https://fonts.googleapis.com/css?family=Roboto:300,700" rel="stylesheet">
	<link rel="stylesheet" type="text/css" href="assets/css/style.css">
    
    <link rel="canonical" href="http://tavernadelasal.com/">
    <link hreflang="es" href="http://tavernadelasal.com/es/" rel="alternate">
    <link hreflang="en" href="http://tavernadelasal.com/en/" rel="alternate">
    <link hreflang="fr" href="http://tavernadelasal.com/fr/" rel="alternate">
    
    <!-- Facebook Open Graph -->
    <meta property="og:type" content="website">
    <meta property="og:url" content="http://tavernadelasal.com/">
    <meta property="og:title" content="Taverna de la Sal · L'Escala">
    <meta property="og:image" content="assets/images/og-image.jpg">
    <meta property="og:description" content="Hotel amb encant a la Costa Brava, situat al casc antic del municipi de l’Escala, Alt Empordà, a pocs metres de la Platja de les Barques, antic port de pescadors.">
    <meta property="og:site_name" content="Taverna de la Sal · L'Escala">
    <meta property="og:locale" content="ca">
    <meta property="og:image:width" content="1200">
    <meta property="og:image:height" content="630">
    
    <!-- Twitter Card 
    <meta name="twitter:card" content="summary">
    <meta name="twitter:site" content="@expoprimats">
    <meta name="twitter:creator" content="@SokvistWeb">
    <meta name="twitter:url" content="http://tavernadelasal.com/">
    <meta name="twitter:title" content="Taverna de la Sal · L'Escala">
    <meta name="twitter:description" content="Hotel amb encant a la Costa Brava, situat al casc antic del municipi de l’Escala, Alt Empordà, a pocs metres de la Platja de les Barques, antic port de pescadors.">
    <meta name="twitter:image" content="assets/images/og-image.jpg">-->
    
    <script async src="assets/js/vendor/modernizr.js"></script>
</head>
    
    
<body class="home" data-region="emea" data-channelcode="tavernadelasaldirect">

    <!-- Header -->
    <header id="header">

        <!-- Logo -->
        <?php include 'includes/logo.php';?>
        
        <!-- Nav -->
        <nav class="main-nav smart-nav icon ion-ios-more" role="navigation">
            <div>
            <ul id="nav">
                <li><a href="#hotel">Hotel</a></li>
                <li><a href="#habitacions">Habitacions</a></li>
                <li><a href="#serveis">Serveis</a></li>
                <li><a href="#galeria">Galeria</a></li>
                <li><a href="#contacte">Contacte</a></li>
            </ul>
            </div>
        </nav>
        
        <div class="contact">
            <a href="tel:0034972776278" class="icon ion-iphone"><span> +34 972 776 278</span></a>
            <a href="mailto:info@tavernadelasal.com" class="icon ion-ios-email"><span class="hidden">info@tavernadelasal.com</span></a>
            <a href="https://www.google.es/maps/place/Carrer+de+Santa+M%C3%A0xima,+7,+17130+L'Escala,+Girona/@42.1259022,3.1312329,17z/data=!3m1!4b1!4m5!3m4!1s0x12ba5f26727f0895:0x3f6c33fc81532504!8m2!3d42.1258982!4d3.1334216" class="icon ion-ios-location" title="Veure a Google maps" target="_blank"><span class="hidden">On som</span></a>
            <a href="#" class="lang-switcher icon ion-earth" title="Idioma"><span class="hidden">Idioma</span></a>
        </div>
        
        <div class="language">
            <ul class="qtranxs_language_chooser" id="qtranslate-chooser">
                <li class="lang-ca active-lang"><a href="ca" hreflang="ca" title="Català"><span>CA</span></a></li>
                <li class="lang-es"><a href="es" hreflang="es" title="Español"><span>ES</span></a></li>
                <li class="lang-en"><a href="en" hreflang="en" title="English"><span>EN</span></a></li>
                <li class="lang-fr"><a href="fr" hreflang="fr" title="Français"><span>FR</span></a></li>
            </ul>
            <div class="qtranxs_widget_end"></div>
        </div>
        
    </header>
    
    
    <div class="book-online">
        <h2>Reserva Online</h2>
        
        <!-- -->
        
        <form action="#" class="ibe" method="get" data-region="emea" data-channelcode="tavernadelasaldirect" data-widget="property_check_availability" target="_blank">
            <fieldset>
                <div class="field">
                    Entrada:
                    <input name="check_in_date" class="datepicker-input" data-language="es" data-position="top right" data-date-format="dd/mm/yyyy" id="check_in_date" value="" type="text" data-init-val="null">
                    Sortida:
                    <input name="check_out_date" class="datepicker-input" data-language="es" data-position="top right" data-date-format="dd/mm/yyyy" id="check_out_date" value="" type="text" data-init-val="null">
                    <input name="commit" value="Reservar" type="submit">
                    
                    <a class="offers" href="popups/offer-ca.html" title="Avantatges" rel="modal:open">Avantatges</a>
                    
                </div>
            </fieldset>
        </form>

    </div><!-- /.book-online -->
    
    <!-- Mobile booking button -->
    <a href="#" class="booking-btn ibe" data-region="emea" data-channelcode="tavernadelasaldirect" target="_blank">Reserva Online</a>
    
    <a href="popups/offer-ca.html" class="advantage-btn" title="Avantatges" rel="modal:open">Avantatges</a>

    
    <div id="btm-frame">
        <h2>Boutique Hotel Only Adults</h2>
        <nav>
            <ul id="arrow-nav" class="arrow-nav">
                <li><a href="#one" class="icon ion-android-arrow-down down bounce" id="one-btn">More</a></li>
                <li><a href="#two" class="icon ion-android-arrow-down down" id="two-btn">Next</a></li>
            </ul>
        </nav>
    </div>
    

    <div id="lft-frame"></div>
    <div id="rgt-frame"></div>
        

    <!-- Intro -->
    <section id="intro" class="fixed">
        <div class="bg-image-bn"></div>
        <div class="bg-image-color"></div>
    </section>

    <!-- One -->
    <section id="one">
    </section>
        
        
    <div class="modernist"></div>


    <!-- two -->
    <section id="hotel" class="two main style3" data-animate-down="arrow-hide" data-animate-up="arrow-show">
        <div class="container container-narrow">
            <div class="row">
                <div class="column fullwidth">
                    <header>
                        <h1>Taverna de la Sal</h1>
                    </header>
                    <div class="copy">
                        <h2>La Taverna de la Sal és l’hotel amb encant a la Costa Brava que esteu buscant</h2>
                        
                        <p>Hotel amb encant a l’Empordà, situat al casc antic del municipi de l’Escala, a 20 metres de la Platja de les Barques, antic port de pescadors.</p>
                        
                    </div>
                </div>
            </div> <!-- /.row -->
            
            <div class="row">
                <div class="column">
                    
                    <?php include 'includes/home-slider.php';?>
                    
                </div>
            </div> <!-- /.row -->
            
            <div class="row">
                <div class="column fullwidth">
                    <div class="copy copy-btm">
                        
                        <p>Al bell mig de platges magnífiques i de paratges naturals únics, podreu gaudir d’una estada en aquest acollidor hotel d’ús exclusiu per a públic adult on el nostre major compromís és el vostre benestar.</p>
                        
                        <p>Hotel amb encant a la Costa Brava- Empordà- L’Escala que consta de 6 precioses habitacions amb un estil mediterrani i que ofereix una cuidada decoració en tots els seus detalls. Un indret on escapar-se...</p>
                        
                    </div>
                </div>
            </div> <!-- /.row -->
        </div> <!-- /.container -->
    </section>


    <!-- three -->
    <section id="habitacions" class="three main style3">
        <div class="modernist"></div>
        <div class="container">
            
            <h2>Habitacions</h2>
            
            <div class="slider" id="basica">
                
                <?php include 'includes/rooms-slider.php';?>
                
                <div class="copy">
                    <h3>Espaioses i acollidores habitacions. Descans, confort i bones sensacions</h3>

                    <p>La Taverna de la Sal és un hotel molt a prop de la platja, romàntic i amb encant de 6 habitacions dobles, cadascuna amb la seva personalitat i cuidades amb tots els detalls. Hi ha habitacions que són convertibles en triples. Totes les habitacions són per a públic adult a partir de 14 anys que desitgi una estada tranquil·la a pocs metres del mar, en ple nucli antic de l’Escala.</p>

                    <p>Àmplies habitacions amb llit de matrimoni. Totes disposen de balcó amb vistes laterals al mar i estan equipades amb:</p>
                    <ul class="custom-li">
                        <li>Llit doble 2,00 x 1,80</li>
                        <li>Aire condicionat i calefacció</li>
                        <li>Bany complet amb dutxa</li>
                        <li>TV pantalla plana (canals internacionals)</li>
                        <li>Caixa de seguretat</li>
                        <li>Mini nevera</li>
                        <li>Assecador de cabell</li>
                        <li>Kettle per cafè i tè</li>
                        <li>Internet Wi-Fi gratis</li>
                        <li>Amenities de bany</li>
                    </ul>

                    <p>Habitacions 100% lliures de fums.</p>

                    <p>No estan permeses les mascotes.</p>

                    <div class="buttons">
                        <a class="booknow ibe" href="#" data-region="emea" data-channelcode="tavernadelasaldirect" target="_blank" title="Pàgina de reserves">Reserva ara</a>
                        <a class="offers offers-rooms" href="popups/offer-ca.html" title="Avantatges" rel="modal:open">Avantatges</a>
                    </div>

                </div>
            </div> <!-- .slider -->
            
            
        </div> <!-- /.container -->
    </section>


    <!-- four -->
    <section id="serveis" class="four main style3">
        <div class="modernist"></div>
        <div class="container">
            <div class="row">
                <div class="column">
                    <header>
                        <h2>Serveis</h2>
                    </header>

                    <div class="copy">
                        <h3>Us oferim tot el necessari perquè volem que la vostra estada a l’Escala sigui única</h3>

                        <h4>Restaurant Taverna de la Sal</h4>
                        <p>El Restaurant està situat a la planta baixa i primera planta de l’edifici. Si bé la nostra especialitat és la carn a la brasa, també oferim una àmplia carta de plats basats en la cuina tradicional catalana, treballant sempre amb productes frescos i de proximitat.</p>

                        <h4>La Terrassa</h4>
                        <p>A la coberta de la Taverna de la Sal s’ubica la Terrassa, un solàrium per als que prefereixen prendre el sol a l’hotel o simplement gaudir de fantàstiques postes de sol que us deixaran enamorats.</p>

                        <h4>Esmorzar</h4>
                        <p>L’esmorzar és tipus buffet i es serveix amb horari de 8h a 10,30h.</p>

                        <h4>Atenció al client</h4>
                        <p>Parlem diferents idiomes i amb dedicació i sempre amb un somriure, oferim una atenció molt personalitzada adaptada a les necessitats de cada client.</p>

                        <p>És sempre un plaer poder recomenar als nostres clients els millors itineraris locals i de la zona, els punts d’interès turístic i els esdeveniments culturals que tenen lloc durant tota l’estada.</p>

                        <h4>Bar-Cafeteria</h4>
                        <p>Al llarg del dia, per als clients que ho desitgin, es mantindrà obert el servei de bar-cafeteria situat al mateix restaurant. Disposem d’una carta de begudes i cocktails que podreu disfrutar quan més us vingui de gust.</p>

                        <h4>Pàrquing</h4>
                        <p>A uns 250 m de l’hotel disposem d’un servei d’aparcament subterrani. És necessària la reserva prèvia i el servei està subjecte a disponibilitat.</p>
                        
                        <p>El preu és de 19,00€/nit (IVA inclòs).</p>
                        
                        <p>Cal sol.licitar-lo en el moment de fer la reserva a l’apartat COMENTARIS. D’aquesta manera els hi donarem prioritat en funció de les sol.licituds i disponiblitat del dia d’arribada.</p>
                    </div>
                    
                </div>
            </div> <!-- /.row -->
        </div> <!-- /.container -->
    </section>
    
    
    <!-- five -->
    <section id="galeria" class="three main style3">
        <div class="modernist"></div>
        <div class="container">
                    
            <h2>Galeria</h2>

            <div class="copy">
                <p>L’Hotel amb encant a la Costa Brava Taverna de la Sal es situa en un entorn privilegiat pels amants de la natura i el mar. L’Empordà en general i l’Escala en particular combinen a la perfecció precioses platges, turisme actiu, diversitat d’esports nàutics, gastronomia de qualitat, cultura i tradició.</p>
            </div>
            
            <?php include 'includes/gallery.php';?>
            
        </div>
    </section>


    <!-- Contact -->
    <aside id="contacte" class="contacte main style3">
        <div class="modernist modernist-light"></div>
        <div class="container">
            
            <h2>Contacte</h2>
            
            <div class="contact-footer">

                <div class="row">
                    <div class="column lft-column">
                        <h3>Taverna de la Sal</h3>
                        <p>Santa Màxima, 7<br>
                        L'Escala,<br>
                        17130 Girona<br>
                        Tel:<a href="tel:0034972776278"> 0034 972 776 278</a><br>
                        <a href="mailto:info@tavernadelasal.com"><span>info@tavernadelasal.com</span></a>
                        </p>
                    </div>
                    <div class="column ctr-column">
                        <!-- Nav Footer -->
                        <nav class="footer-nav" role="navigation">
                            <ul>
                                <li><a href="#hotel">Hotel</a></li>
                                <li><a href="#habitacions">Habitacions</a></li>
                                <li><a href="#serveis">Serveis</a></li>
                                <li><a href="#galeria">Galeria</a></li>
                                <li><a href="#contacte">Contacte</a></li>
                            </ul>
                        </nav>
                    </div>
                    <div class="column rgt-column">
                        <!-- Mailchimp Subscribe Form -->
                        <div id="mc_embed_signup" class="subscribe-form">
                            <h3>Subscriu-te i t'informarem de les nostres ofertes i novetats</h3>
                            <form action="#" method="post" id="mc-embedded-subscribe-form" name="mc-embedded-subscribe-form" class="validate" target="_blank" novalidate="">

                                <div class="mc-field-group">
                                    <input type="email" value="" name="EMAIL" class="required email" id="mce-EMAIL" placeholder="Email">
                                </div>

                                <input type="text" value="" name="FNAME" class="" id="mce-FNAME" placeholder="Nom">
                                <div id="mce-responses" class="clear">
                                    <div class="response" id="mce-error-response"></div>
                                    <div class="response" id="mce-success-response"></div>
                                </div>    <!-- real people should not fill this in and expect good things - do not remove this or risk form bot signups-->
                                <div id="real-people" aria-hidden="true"><input type="text" name="b_e5b778581d428a50593d91c5f_e98b7563d8" tabindex="-1" value=""></div>
                                <div class="clear">
                                    <input type="submit" value="Enviar" name="subscribe" id="mc-embedded-subscribe" class="button">
                                </div>
                            </form>
                        </div> <!-- /.subscribe-form -->
                        <!-- Icons -->
                        <ul class="actions">
                            <li><a href="https://www.facebook.com/tavernadelasal/" target="_blank" title="Facebook" class="icon ion-social-facebook"><span class="label">Facebook</span></a></li>
                            <li><a href="https://www.instagram.com/tavernadelasal/" target="_blank" title="Instagram" class="icon ion-social-instagram"><span class="label">Instagram</span></a></li>
                        </ul>
                    </div>
                </div> <!-- /.row -->
                
                <div class="row logos-row">
                    <!--<ul class="logos">
                        <li><img src="assets/images/logo-petits-grans-hotels.png" alt="Costa Brava Hotels" width="109" height="60">
                        </li>
                        <li><img src="assets/images/logo-natura-i-turisme-actiu.png" alt="Costa Brava Pirineu de Girona" width="227" height="60">
                        </li>
                        <li><img src="assets/images/logo-bed-and-bike.png" alt="Bike & Bed" width="78" height="60"></li>
                    </ul>-->
                </div> <!-- /.row -->

            </div> <!-- /.contact-footer -->
        </div> <!-- /.container -->
    </aside>


    <!-- Footer -->
    <footer id="footer" class="">

        <!-- Menu -->
        <ul class="menu">
            <li>&copy; Taverna de la Sal</li>
            <!--<li>HG-000000</li>-->
            <li><a href="popups/legal-ca.html" class="legal" title="Avís legal" rel="modal:open">Avís Legal</a></li>
            <li>Design: <a href="http://sokvist.com" title="Sokvist, Web & SEO">Sokvist</a></li>
        </ul>
        
        <a id="back-to-top" href="#one" title="Back to top"><span>To top</span></a>

    </footer>
    


    <script src="assets/js/vendor/jquery-1.11.2.min.js"></script>
    <script src="assets/js/plugins.js"></script>
    <script src="assets/js/js/main.min.js"></script>

    <script>
        /*(function(i,s,o,g,r,a,m){i['GoogleAnalyticsObject']=r;i[r]=i[r]||function(){
        (i[r].q=i[r].q||[]).push(arguments)},i[r].l=1*new Date();a=s.createElement(o),
        m=s.getElementsByTagName(o)[0];a.async=1;a.src=g;m.parentNode.insertBefore(a,m)
        })(window,document,'script','//www.google-analytics.com/analytics.js','ga');

        ga('create', 'UA-47544981-1', 'sokvist.com');
        ga('send', 'pageview');*/
    </script>
    
    <script src="//widget.siteminder.com/ibe.min.js"></script>

</body>
</html>